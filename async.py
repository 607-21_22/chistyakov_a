import aiohttp
import asyncio

async def get_book_info(book_title):
    base_url = "https://www.googleapis.com/books/v1/volumes"
    params = {
        'q': book_title,
    }

    async with aiohttp.ClientSession() as session:
        async with session.get(base_url, params=params) as response:
            data = await response.json()
            return data

async def main():
    book_title = 'Harry Potter'

    try:
        book_info = await get_book_info(book_title)
        if 'items' in book_info:
            for item in book_info['items']:
                volume_info = item.get('volumeInfo', {})
                print(f"Title: {volume_info.get('title', 'N/A')}")
                print(f"Authors: {', '.join(volume_info.get('authors', ['N/A']))}")
                print(f"Description: {volume_info.get('description', 'N/A')}")
                print("------")
        else:
            print("No books found.")
    except Exception as e:
        print("Error:", e)

if __name__ == "__main__":
    asyncio.run(main())